package spring.boot.API.DTO;

import java.sql.Date;

import spring.boot.API.entity.Timesheet;

public class TimesheetDTO {
    private Long id;
    private String project;
    private String task;
    private Date dateFrom;
    private Date dateTo;
    private String assignTo;
    private String status;

    public TimesheetDTO(Timesheet timesheet) {
        this.id = timesheet.getId();
        this.project = timesheet.getProject();
        this.task = timesheet.getTask();
        this.dateFrom = timesheet.getDateFrom();
        this.dateTo = timesheet.getDateTo();
        this.assignTo = timesheet.getUser().getName();
        this.status = timesheet.getStatus().getStatus();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProject() {
        return project;
    }

    public void setProject(String project) {
        this.project = project;
    }

    public String getTask() {
        return task;
    }

    public void setTask(String task) {
        this.task = task;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public String getAssignTo() {
        return assignTo;
    }

    public void setAssignTo(String assignTo) {
        this.assignTo = assignTo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
