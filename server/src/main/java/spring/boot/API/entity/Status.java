package spring.boot.API.entity;

import java.util.List;

import jakarta.persistence.CascadeType;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "status")
public class Status {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    public Long id;

    @Column(nullable = false, unique = true)
    public String status;

    @OneToMany(mappedBy = "status", cascade = CascadeType.ALL)
    private List<Timesheet> timesheets;

    /* getters and setters */

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "StatusEntity [Id=" + id + ", Status=" + status + "]";
    }

    // public List<Timesheet> getTimesheets() {
    //     return timesheets;
    // }

    // public void setTimesheets(List<Timesheet> timesheets) {
    //     this.timesheets = timesheets;
    // }
}
