package spring.boot.API.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jakarta.transaction.Transactional;
import spring.boot.API.entity.Status;
import spring.boot.API.repository.StatusRepository;

@Service
public class StatusService {

    @Autowired
    private StatusRepository statusRepo;

    private String status404 = "Status not exist in database";

    @Transactional
    public List<Status> getAllStatus() {
        return statusRepo.findAll();
    }

    @Transactional
    public Optional<Status> getStatusById(Long id) {
        return statusRepo.findById(id);
    }

    @Transactional
    public Status createStatus(Status status) {
        return statusRepo.save(status);
    }

    @Transactional
    public String deleteStatusById(Long id) {
        Status existingStatus = statusRepo.findById(id).orElse(null);
        if (existingStatus == null) {
            return status404;
        } else {
            statusRepo.deleteById(id);
            return "Status successfully deleted";
        }
    }

    @Transactional
    public String updateStatusById(Status status) {
        Status existingStatus = statusRepo.findById(status.id).orElse(null);
        String strStatus = status.toString();
        if (existingStatus == null) {
            return status404;
        } else {
            statusRepo.save(status);
            return "status succesfully updated" + strStatus;
        }
    }
}
