package spring.boot.API.service;

import java.util.Optional;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import spring.boot.API.entity.User;
import spring.boot.API.repository.UserRepository;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;
    private String noUser = "User not found!";

    @Transactional
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    @Transactional
    public Optional<User> getUserById(Long id) {
        return userRepository.findById(id);
    }

    @Transactional
    public User createUser(User user) {
        return userRepository.save(user);
    }

    @Transactional
    public String updateUserById(User user) {
        String userString = user.toString();
        User existingUser = userRepository.findById(user.id).orElse(null);

        if (existingUser != null) {
            userRepository.save(user);
            return "User succesfully update!" + userString;
        } else {
            return noUser;
        }
    }

    @Transactional
    public String deleteUserById(Long id) {
        User existingUser = userRepository.findById(id).orElse(null);

        if (existingUser != null) {
            userRepository.deleteById(id);
            return "User succesfully deleted from database!";
        } else {
            return noUser;
        }

    }

}
