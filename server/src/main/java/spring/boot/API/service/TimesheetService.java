package spring.boot.API.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import jakarta.transaction.Transactional;
import spring.boot.API.DTO.TimesheetDTO;
import spring.boot.API.entity.Timesheet;
import spring.boot.API.repository.TimesheetRepository;

@Service
public class TimesheetService {

    private String timesheet404 = "Timesheet not exist in the database";
    @Autowired
    private TimesheetRepository timesheetRepo;

    // @Autowired
    // private UserService userService;

    // @Autowired
    // private StatusService statusService;

    // private void checkUserOrStatusIdAvailable(Timesheet timesheet) {
    // Optional<User> user = userService.getUserById(timesheet.getUser().getId());
    // Optional<Status> status =
    // statusService.getStatusById(timesheet.getStatus().getId());
    // if (!user.isPresent() || !status.isPresent()) {
    // throw new Error("Invalid user or status reference");
    // }
    // timesheet.setUser(user.get());
    // timesheet.setStatus(status.get());
    // }

    @Transactional
    public List<Timesheet> getAllTimesheet() {
        return timesheetRepo.findAll();
    }

    @Transactional
    public List<Timesheet> getAllTimesheetSortAsc() {
        Sort sortDateFrom = Sort.by("dateFrom").ascending();
        return timesheetRepo.findAll(sortDateFrom);
    }

    @Transactional
    public List<Timesheet> getAllTimesheetSortDesc() {
        Sort sortDateFrom = Sort.by("dateFrom").descending();
        return timesheetRepo.findAll(sortDateFrom);
    }

    @Transactional
    public List<TimesheetDTO> getAllTimesheetJSON() {
        List<Timesheet> timesheets = timesheetRepo.findAll();
        List<TimesheetDTO> dtos = new ArrayList<>();

        for (Timesheet timesheet : timesheets) {
            System.out.println(timesheet);
            dtos.add(new TimesheetDTO(timesheet));
        }
        return dtos;
    }

    @Transactional
    public Optional<Timesheet> getTimesheetById(Long id) {
        return timesheetRepo.findById(id);
    }

    @Transactional
    public Timesheet createTimesheet(Timesheet timesheet) {
        // checkUserOrStatusIdAvailable(timesheet);
        return timesheetRepo.save(timesheet);
    }

    @Transactional
    public String updateTimesheet(Timesheet timesheet) {
        Timesheet isExist = timesheetRepo.findById(timesheet.id).orElse(null);

        if (isExist == null) {
            return timesheet404;
        }
        // checkUserOrStatusIdAvailable(timesheet);

        timesheetRepo.save(timesheet);
        return "Timesheet successfully updated" + getTimesheetById(timesheet.getId());
    }

    @Transactional
    public String deleteTimesheet(Long id) {
        Timesheet isExist = timesheetRepo.findById(id).orElse(null);
        if (isExist == null) {
            return timesheet404;
        } else {
            timesheetRepo.deleteById(id);
            return "Timesheet successfully deleted";
        }
    }
}
