package spring.boot.API.configurations;

import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin(origins = "http://localhost:4200/")
public interface CorsConfig {

}
