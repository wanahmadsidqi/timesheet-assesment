package spring.boot.API.controller;

import org.springframework.web.bind.annotation.RestController;

import spring.boot.API.DTO.TimesheetDTO;
import spring.boot.API.configurations.CorsConfig;
import spring.boot.API.entity.Timesheet;
import spring.boot.API.service.TimesheetService;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.PathVariable;

@RestController
@RequestMapping("/timesheet-api")
public class TimesheetController implements CorsConfig {

    @Autowired
    private TimesheetService timesheetService;

    @GetMapping("/")
    public List<Timesheet> getAllTimesheet() {
        return timesheetService.getAllTimesheet();
    }

    @GetMapping("/asc")
    public List<Timesheet> getAllTimesheetSortAsc() {
        return timesheetService.getAllTimesheetSortAsc();
    }

    @GetMapping("/desc")
    public List<Timesheet> getAllTimesheetSortDesc() {
        return timesheetService.getAllTimesheetSortDesc();
    }

    @GetMapping("/json")
    public List<TimesheetDTO> getAllTimesheetJSON() {
        return timesheetService.getAllTimesheetJSON();
    }

    @GetMapping("/timesheet/{id}")
    public Optional<Timesheet> getTimesheetById(@PathVariable Long id) {
        return timesheetService.getTimesheetById(id);
    }

    @PostMapping("/create")
    public Timesheet createTimesheet(@RequestBody Timesheet timesheet) {
        return timesheetService.createTimesheet(timesheet);
    }

    @PutMapping("/update")
    public String updateTimesheetById(@RequestBody Timesheet timesheet) {
        return timesheetService.updateTimesheet(timesheet);
    }

    @DeleteMapping("/timesheet/{id}")
    public String deleteTimesheetById(@PathVariable Long id) {
        return timesheetService.deleteTimesheet(id);
    }

}
