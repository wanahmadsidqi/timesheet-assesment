package spring.boot.API.controller;

import org.springframework.web.bind.annotation.RestController;

import spring.boot.API.configurations.CorsConfig;
import spring.boot.API.entity.User;
import spring.boot.API.service.UserService;

import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PutMapping;

@RestController
@RequestMapping("/user-api")
public class UserController implements CorsConfig {

    @Autowired
    private UserService userService;

    @GetMapping("/")
    public List<User> getAllUsers() {
        return userService.getAllUsers();
    }

    @GetMapping("/user-id/{id}")
    public Optional<User> getUserById(@PathVariable Long id) {
        return userService.getUserById(id);
    }

    @PostMapping("/create")
    public User createUser(@RequestBody User newUser) {
        userService.createUser(newUser);
        return newUser;
    }

    @PutMapping("/update")
    public String updateUser(@RequestBody User user) {
        return userService.updateUserById(user);
    }

    @DeleteMapping("/user-id/{id}")
    public String deleteUser(@PathVariable Long id) {
        return userService.deleteUserById(id);
    }

}
