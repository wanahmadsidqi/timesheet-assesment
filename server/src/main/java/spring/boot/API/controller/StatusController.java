package spring.boot.API.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import spring.boot.API.configurations.CorsConfig;
import spring.boot.API.entity.Status;
import spring.boot.API.service.StatusService;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PostMapping;

@RestController
@RequestMapping("/status-api")

public class StatusController implements CorsConfig {

    @Autowired
    private StatusService statusService;

    @GetMapping("/")
    public List<Status> getAllStatus() {
        return statusService.getAllStatus();
    }

    @GetMapping("/status-id/{id}")
    public Optional<Status> getStatusById(@PathVariable Long id) {
        return statusService.getStatusById(id);
    }

    @PutMapping("/update")
    public String updateStatusById(@RequestBody Status status) {
        return statusService.updateStatusById(status);
    }

    @PostMapping("/create")
    public Status createStatus(@RequestBody Status status) {
        return statusService.createStatus(status);
    }

    @DeleteMapping("/status-id/{id}")
    public String deleteStatusById(@PathVariable Long id) {
        return statusService.deleteStatusById(id);
    }

}
