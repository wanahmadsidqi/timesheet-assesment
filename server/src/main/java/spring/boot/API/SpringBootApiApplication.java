package spring.boot.API;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootApiApplication.class, args);
		System.out.println("|##############################|");
		System.out.println("|                              |");
		System.out.println("|                              |");
		System.out.println("|                              |");
		System.out.println("|                              |");
		System.out.println("|                              |");
		System.out.println("|                              |");
		System.out.println("|  WELCOME TO SPRING BOOT API  |");
		System.out.println("|                              |");
		System.out.println("|                              |");
		System.out.println("|                              |");
		System.out.println("|                              |");
		System.out.println("|                              |");
		System.out.println("|                              |");
		System.out.println("|##############################|");
	}

}
