package spring.boot.API.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import spring.boot.API.entity.Status;

public interface StatusRepository extends JpaRepository<Status, Long> {

}
