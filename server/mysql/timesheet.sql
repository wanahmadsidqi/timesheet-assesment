CREATE TABLE timesheet (
    id INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    project VARCHAR(255) NOT NULL,
    task VARCHAR(255) NOT NULL,
    date_from DATE NOT NULL,
    date_to DATE NOT NULL,
    status_id INT NOT NULL,
    user_id INT NOT NULL,
    FOREIGN KEY (status_id) REFERENCES status(id) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (user_id) REFERENCES user(id) ON DELETE CASCADE ON UPDATE CASCADE
);

INSERT INTO timesheet (project, task, date_from, date_to, status_id, user_id) VALUES 
('Angular UI', 'Task 1', '2024-04-15', '2024-04-17', 1, 1),
('Create Database', 'Task 2', '2024-04-16', '2024-04-18', 2, 2);

SELECT t.id AS id, t.project, t.task, t.dateFrom, t.dateTo, s.status, u.name AS assignTo
FROM timesheet t
JOIN status s ON t.status_id = s.id
JOIN user u ON t.user_id = u.id;
