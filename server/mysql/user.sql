USE spring_boot_api_db;

CREATE TABLE User (
    id INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    name VARCHAR(255) UNIQUE NOT NULL
);

INSERT INTO User (name) VALUES 
('Amelia Johnson'),
('Noah Martinez'),
('Liam Thompson'),
('Olivia Garcia'),
('Sophia Lee');