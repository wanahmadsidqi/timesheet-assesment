CREATE TABLE Status (
    id INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    status VARCHAR(255) UNIQUE NOT NULL
);

INSERT INTO Status (status) VALUES 
('Open'),
('In Progress'),
('Closed');