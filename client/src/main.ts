import { bootstrapApplication } from '@angular/platform-browser';
import { appConfig } from './app/app.config';
import { AppComponent } from './app/app.component';

bootstrapApplication(AppComponent, appConfig)
  .catch((err) => console.error(err));

// import { bootstrapApplication } from '@angular/platform-browser';
// import { AppComponent } from './app/app.component';
// import { HttpClientModule } from '@angular/common/http'; // Import HttpClientModule

// bootstrapApplication(AppComponent, {
//   providers: [HttpClientModule], // Provide HttpClient
// })
//   .then(() => console.log('Application bootstrapped successfully!'))
//   .catch((err) => console.error('Error bootstrapping application:', err));
