import { CommonModule } from '@angular/common';
import { Component, Input } from '@angular/core';
import {
  FormControl,
  FormGroup,
  FormsModule,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { Subscription } from 'rxjs';
import { APIService } from '../api.service';
import { Status, TimesheetInterface, User } from '../interface';

@Component({
  selector: 'app-timesheet',
  standalone: true,
  imports: [FormsModule, CommonModule, ReactiveFormsModule],
  templateUrl: './timesheet.component.html',
  styleUrl: './timesheet.component.css',
})
export class TimesheetComponent {
  APIsubscription: Subscription | undefined;
  searchInput = '';
  showModal = false;
  data?: TimesheetInterface[];

  timesheets: TimesheetInterface[] = [];

  users: User[] = [];
  status: Status[] = [];
  fields: any;
  selectTimesheet: TimesheetInterface | null = null;

  form = new FormGroup({
    project: new FormControl('', Validators.required),
    task: new FormControl('', Validators.required),
    dateFrom: new FormControl('', Validators.required),
    dateTo: new FormControl('', Validators.required),
    status: new FormControl('', Validators.required),
    assignTo: new FormControl('', Validators.required),
  });

  constructor(private apiService: APIService) {}

  filterSearch() {
    const input = this.searchInput.toLowerCase();

    const filtered = this.data?.filter((timesheet) => {
      return timesheet.task.toLowerCase().includes(input);
    });
    this.timesheets = filtered as TimesheetInterface[];
  }

  async ngOnInit() {
    await this.apiService.getAllTimesheet();
    this.users = await this.apiService.getAllUsers();
    this.status = await this.apiService.getAllStatus();

    this.APIsubscription = this.apiService.timesheet$.subscribe((data) => {
      this.data = data;
      this.timesheets = data;
    });

    this.apiService.selectTimesheet$.subscribe(
      (timesheet) => (this.selectTimesheet = timesheet)
    );

    this.fields = [
      { name: 'project', input: 'text' },
      { name: 'task', input: 'text' },
      { name: 'dateFrom', input: 'date' },
      { name: 'dateTo', input: 'date' },
      { name: 'status', status: this.status },
      { name: 'assignTo', user: this.users },
    ];
  }

  ngOnDestroy() {
    this.APIsubscription?.unsubscribe();
  }

  async deleteTimesheet(id: number) {
    this.apiService.deleteTimesheet(id);
  }

  editTimesheet(ts: TimesheetInterface) {
    this.openModal();
    this.apiService.setSelectTimesheet$(ts);

    this.form.patchValue({
      project: this.selectTimesheet?.project,
      assignTo: this.selectTimesheet?.user.id?.toString(),
      dateFrom: this.selectTimesheet?.dateFrom,
      dateTo: this.selectTimesheet?.dateTo,
      status: this.selectTimesheet?.status.id?.toString(),
      task: this.selectTimesheet?.task,
    });
  }

  resetForm() {
    this.form.patchValue({
      assignTo: '',
      project: '',
      dateFrom: '',
      dateTo: '',
      status: '',
      task: '',
    });
  }

  openModal() {
    this.showModal = true;
  }

  closeModal() {
    this.showModal = false;
    this.resetForm();
    this.apiService.setSelectTimesheet$(null);
  }

  debug() {
    // console.log(this.timesheet);
    // console.log(this.form.valid);
    // console.log(this.form.value);
    // console.log(this.subscription);
  }

  async saveTimesheet() {
    const { assignTo, dateFrom, dateTo, project, status, task } =
      this.form.value;

    const data: Partial<TimesheetInterface> = {
      project: project as string,
      task: task as string,
      dateFrom: dateFrom as string,
      dateTo: dateTo as string,
      status: { id: Number(status) },
      user: { id: Number(assignTo) },
    };

    if (this.selectTimesheet == null) {
      console.log('create');
      await this.apiService.createTimesheet(data);
    } else {
      data.id = Number(this.selectTimesheet.id);
      console.log('update');
      await this.apiService.updateTimesheet(data);
    }
  }

  async sortTimesheetByDateAsc() {
    this.apiService.sortTimesheetByDateAsc();
  }

  async sortTimesheetByDateDesc() {
    this.apiService.sortTimesheetByDateDesc();
  }

  async submitForm() {
    if (this.form.valid) {
      try {
        await this.saveTimesheet();
        await this.apiService.getAllTimesheet();
        this.closeModal();
      } catch (error) {
        console.log(error);
        alert('error creating timesheet!');
      }
    } else {
      alert('Fill in the forms!');
    }
  }
}
