import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

interface EditForm {
  assignTo: string;
  dateFrom: string;
  dateTo: string;
  project: string;
  status: string;
  task: string;
}

interface State {
  showModal: boolean;
  // editForm: EditForm;
}

@Injectable({
  providedIn: 'root',
})
export class DataShareService {
  form = {
    assignTo: '',
    dateFrom: '',
    dateTo: '',
    project: '',
    status: '',
    task: '',
  };

  initialState: State = {
    showModal: false,
    // editForm: this.form,
  };

  private stateSubject = new BehaviorSubject<State>(this.initialState);
  public state$: Observable<State>;
  currentState = this.stateSubject.getValue();

  openModal() {
    this.stateSubject.next({ ...this.currentState, showModal: true });
  }

  closeModal() {
    this.stateSubject.next({ ...this.currentState, showModal: false });
  }

  constructor() {
    this.state$ = this.stateSubject.asObservable();
  }
}
