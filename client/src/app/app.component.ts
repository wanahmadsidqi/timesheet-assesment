import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { APIService } from './api.service';
import { TimesheetComponent } from './timesheet/timesheet.component';

@Component({
  selector: 'root',
  standalone: true,
  imports: [
    RouterOutlet,
    // MenuComponent,
    // ModalformComponent,
    TimesheetComponent,
  ],
  providers: [APIService],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css',
})
export class AppComponent {
  title = 'main component (app.component.ts)';
}
