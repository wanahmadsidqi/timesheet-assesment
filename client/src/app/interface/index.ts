export interface TimesheetInterface {
  id: number;
  project: string;
  task: string;
  dateFrom: string;
  dateTo: string;
  user: User;
  status: Status;
}

export interface User {
  id: number;
  name?: string;
}

export interface Status {
  id: number;
  status?: string;
}
