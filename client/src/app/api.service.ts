import { Injectable } from '@angular/core';
import axios from 'axios';
import { Status, TimesheetInterface, User } from './interface';
import { BehaviorSubject, Observable } from 'rxjs';
import { FormGroup } from '@angular/forms';

@Injectable({
  providedIn: 'root',
})
export class APIService {
  datasource = axios.create({
    baseURL: 'http://localhost:8080',
    timeout: 5000,
  });

  private timesheetSubject = new BehaviorSubject<TimesheetInterface[]>([]);
  private selectTimesheetSubject =
    new BehaviorSubject<TimesheetInterface | null>(null);
  private usersSubject = new BehaviorSubject<User[]>([]);
  private statusSubject = new BehaviorSubject<Status[]>([]);

  public timesheet$: Observable<TimesheetInterface[]>;
  public selectTimesheet$: Observable<TimesheetInterface | null>;
  public users$: Observable<User[]>;
  public status$: Observable<Status[]>;

  setTimesheets$(newTimesheets: TimesheetInterface[]) {
    this.timesheetSubject.next(newTimesheets);
  }

  setSelectTimesheet$(timesheet: TimesheetInterface | null) {
    this.selectTimesheetSubject.next(timesheet);
  }

  setUsers$(users: User[]) {
    this.usersSubject.next(users);
  }

  setStatus$(status: Status[]) {
    this.statusSubject.next(status);
  }

  constructor() {
    this.timesheet$ = this.timesheetSubject.asObservable();
    this.selectTimesheet$ = this.selectTimesheetSubject.asObservable();
    this.users$ = this.usersSubject.asObservable();
    this.status$ = this.statusSubject.asObservable();
  }

  async getAllUsers() {
    try {
      const response = await this.datasource({
        method: 'get',
        url: '/user-api/',
      });
      this.setUsers$(response.data);
      return response.data as User[];
    } catch (error) {
      console.log(error);
      return [];
    }
  }

  async getAllStatus() {
    try {
      const response = await this.datasource({
        method: 'get',
        url: '/status-api/',
      });
      this.setStatus$(response.data);
      return response.data as Status[];
    } catch (error) {
      console.log(error);
      return [];
    }
  }

  async getAllTimesheet() {
    try {
      const response = await this.datasource({
        method: 'get',
        url: '/timesheet-api/',
      });
      const timesheets = response.data as TimesheetInterface[];
      this.timesheetSubject.next(timesheets);
      return timesheets;
    } catch (error) {
      console.log(error);
      return [];
    }
  }

  async createTimesheet(data: Partial<TimesheetInterface>) {
    await this.datasource({
      url: 'timesheet-api/create',
      method: 'post',
      data,
    });
    await this.getAllTimesheet();
  }

  async updateTimesheet(data: Partial<TimesheetInterface>) {
    await this.datasource({
      method: 'put',
      url: `timesheet-api/update`,
      data,
    });
  }

  async deleteTimesheet(id: number) {
    const response = await this.datasource({
      method: 'delete',
      url: `timesheet-api/timesheet/${id}`,
    });
    await this.getAllTimesheet();
    return response.data;
  }

  async sortTimesheetByDateAsc() {
    try {
      const response = await this.datasource({
        url: '/timesheet-api/asc',
        method: 'get',
      });
      const data = response.data;
      this.setTimesheets$(data);
    } catch (error) {
      console.log(error);
    }
  }
  
  async sortTimesheetByDateDesc() {
    try {
      const response = await this.datasource({
        url: '/timesheet-api/desc',
        method: 'get',
      });
      const data = response.data;
      this.setTimesheets$(data);
    } catch (error) {
      console.log(error);
    }
  }
}
